package main

import (
	"errors"
	"fmt"
	"os"
	"time"
)

func main() {
	args := os.Args
	printline := fmt.Println
	currentHour := time.Now().Hour()
	greeting, err := getGreeting(currentHour)

	if err != nil {
		printline(err)
		os.Exit(1)
	}

	if len(args) > 1 {
		printline(greeting, args[1])
	} else {
		printline(greeting, "Hello, I am Gopher")
	}
}

/**
 * Get Greeting
 */
func getGreeting(hour int) (string, error) {
	var message string

	if hour < 7 {
		err := errors.New("Its too early for greetings...")
		return message, err
	}

	if hour < 12 {
		message = "Good Morning..."
	} else if hour < 18 {
		message = "Good Afternoon..."
	} else {
		message = "Good Evening..."
	}

	return message, nil
}
